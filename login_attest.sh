#!/bin/sh

printf "\033[30;41m"
echo "                                                                                                                                                                      "
echo "      ▄▄▄▄▄  ▄▄   ▄▄▄▄▄▄▄▄ ▄▄▄▄▄▄   ▄▄▄  ▄▄▄▄▄  ▄▄▄▄▄ ▄▄▄▄▄▄▄▄     ▄          ▄▄  ▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄▄▄▄▄▄  ▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄▄▄    ▄▄  ▄▄▄▄▄▄▄ ▄▄▄▄▄   ▄▄▄▄  ▄▄   ▄       "
echo "        █    █▀▄  █   █    █      ▄▀   ▀ █   ▀█   █      █    ▀▄ ▄▀           ██     █      █    █      █▀   ▀   █      █      ██     █      █    ▄▀  ▀▄ █▀▄  █       "
echo "        █    █ █▄ █   █    █▄▄▄▄▄ █   ▄▄ █▄▄▄▄▀   █      █     ▀█▀           █  █    █      █    █▄▄▄▄▄ ▀█▄▄▄    █      █     █  █    █      █    █    █ █ █▄ █       "
echo "        █    █  █ █   █    █      █    █ █   ▀▄   █      █      █            █▄▄█    █      █    █          ▀█   █      █     █▄▄█    █      █    █    █ █  █ █       "
echo "      ▄▄█▄▄  █   ██   █    █▄▄▄▄▄  ▀▄▄▄▀ █    ▀ ▄▄█▄▄    █      █           █    █   █      █    █▄▄▄▄▄ ▀▄▄▄█▀   █    ▄▄█▄▄  █    █   █    ▄▄█▄▄   █▄▄█  █   ██       "
echo "                                                                                                                                                                      "
echo "                                                                                                                                                                      "
printf "\033[0;0m"

read -r -p "Nonce: " nonce_input

# remove trailing newline before converting to hex
# then remove trailing whitespaces added by xxd
nonce="$(echo "${nonce_input}" | tr -d '\n' | xxd -p | tr -d ' ')"

PCR_LIST="sha256:0,1,2,3,4,5,6,7"

/usr/sbin/bauen1-sbt attest "${PCR_LIST}" "${nonce}"

# Cleanup
rm -f /tmp/quote.sig /tmp/quote.msg /tmp/quote.pcrs

sleep 120
