# Personal SecureBoot and TPM2.0 Attestiation helpers

## SecureBoot Setup

Ensure that `/boot` is encrypted

1. Boot into "Administer Secure Boot"
2. Select "Erase all secure boot settings"
3. Save and exit
4. Boot into debian
5. Confirm that `SetupMode=1`:

    ```sh
	efivar --name="8be4df61-93ca-11d2-aa0d-00e098032b8c-SetupMode"
    ```

6. Confirm empty SecureBoot Variables

    ```sh
    efi-readvar
    ```

7. Generate all necessary keys

    ```sh
    bauen1-sbt generate-keys
    ```

8. Enroll PK, enabling SecureBoot, switching to `SetupMode=0`

    ```sbt
    bauen1-sbt enter-usermode
    ```

9. Confirm that `SetupMode=1`

    ```sh
    efivar --name="8be4df61-93ca-11d2-aa0d-00e098032b8c-SetupMode"
    ```

10. Enroll KEK, db

    ```sh
    bauen1-sbt deploy-keys
    ```

11. Build the image to boot

    ```sh
    bauen1-sbt build-image "$(uname -r)" normal
    ```

12. Deploy the image (adding the hash to `db`)

    ```sh
    bauen1-sbt deploy-image "$(uname -r)" normal
    ```

13. Add BootEntry

    ```sh
    efibootmgr --create --bootnum B001 --label "sbt-$(uname -r)-normal" --disk="$(grub-probe --target-device /boot/efi)" --loader "sbt-$(uname -r)-normal.efi"
    ```

14. Reboot
15. Note down new PCR Values
16. Verify SecureBoot Result

    ```sh
    efi-readvars
    ```

## Update workflow

1. Retire oldest set of images (removing hashes from `db`)
2. Build new images
3. Deploy new images
4. Reboot

## TODO

Utilize https://github.com/grawity/tpm_futurepcr/tree/master/tpm_futurepcr to fully predict next boot PCRs

```sh
efi-updatevar -k KEK.key -c db.crt db # wipe db and add db.crt to signature list
pesign --hash --in sbt-5.10.0-7-amd64-normal.efi | awk '{ print $2 }' | xxd -r -p > test5.sha256 # no --padding is important
sbsiglist --owner "$(< guid.txt)" --type sha256 --output test5.esl test5.sha256
efi-updatevar -k KEK.key -a -e -f test5.esl db # append to db using esl (esl is apparently very important)
```
