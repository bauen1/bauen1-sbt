#!/bin/sh

PREREQ="udev"

prereqs() {
    echo "$PREREQ"
}

case "$1" in
    prereqs)
        prereqs
        exit 0
    ;;
esac

# for the QR code we need a font with support for box characters
setfont /usr/share/consolefonts/spleen-8x16.psfu.gz

printf "\033[30;41m"
echo "                                                                                                                                                                      "
echo "      ▄▄▄▄▄  ▄▄   ▄▄▄▄▄▄▄▄ ▄▄▄▄▄▄   ▄▄▄  ▄▄▄▄▄  ▄▄▄▄▄ ▄▄▄▄▄▄▄▄     ▄          ▄▄  ▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ▄▄▄▄▄▄  ▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄▄▄    ▄▄  ▄▄▄▄▄▄▄ ▄▄▄▄▄   ▄▄▄▄  ▄▄   ▄       "
echo "        █    █▀▄  █   █    █      ▄▀   ▀ █   ▀█   █      █    ▀▄ ▄▀           ██     █      █    █      █▀   ▀   █      █      ██     █      █    ▄▀  ▀▄ █▀▄  █       "
echo "        █    █ █▄ █   █    █▄▄▄▄▄ █   ▄▄ █▄▄▄▄▀   █      █     ▀█▀           █  █    █      █    █▄▄▄▄▄ ▀█▄▄▄    █      █     █  █    █      █    █    █ █ █▄ █       "
echo "        █    █  █ █   █    █      █    █ █   ▀▄   █      █      █            █▄▄█    █      █    █          ▀█   █      █     █▄▄█    █      █    █    █ █  █ █       "
echo "      ▄▄█▄▄  █   ██   █    █▄▄▄▄▄  ▀▄▄▄▀ █    ▀ ▄▄█▄▄    █      █           █    █   █      █    █▄▄▄▄▄ ▀▄▄▄█▀   █    ▄▄█▄▄  █    █   █    ▄▄█▄▄   █▄▄█  █   ██       "
echo "                                                                                                                                                                      "
echo "                                                                                                                                                                      "
printf "\033[0;0m"

read -p "Nonce: " nonce_input

# remove trailing newline before converting to hex
# then remove trailing whitespaces added by xxd
nonce="$(echo "${nonce_input}" | tr -d '\n' | xxd -p | tr -d ' ')"

PCR_LIST="sha256:0,1,2,3,4,5,6,7"
AK_CONTEXT_PERSISTENT="0x81010003"

# see bauen1-sbt attest
tpm2 quote \
    --key-context="${AK_CONTEXT_PERSISTENT}" \
    --pcr-list="${PCR_LIST}" \
    --qualification="${nonce}" \
    --message="/tmp/quote.msg" \
    --signature="/tmp/quote.sig" \
    --format=plain \
    --hash-algorithm=sha256

tpm2 pcrread \
    "${PCR_LIST}" \
    --output "/tmp/quote.pcrs"

cat /tmp/quote.sig /tmp/quote.msg /tmp/quote.pcrs | base64 --wrap=0 | qrencode -t ANSIUTF8

# Cleanup
rm -f /tmp/quote.sig /tmp/quote.msg /tmp/quote.pcrs
