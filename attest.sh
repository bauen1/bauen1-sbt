#!/bin/bash

readonly AK_CTX=0x81010003
readonly PCR_LIST="sha256:0,1,2,3,4,5,6,7"

read -p "Nonce: " -r nonce_input

readonly nonce_input

# remove newline from nonce, we don't use it
nonce="$(echo "${nonce_input}" | tr --delete '\n' | xxd -p)"
readonly nonce

echo "Nonce: ${nonce}"

# TODO: rewrite to use additional fds for message, signature
tpm2_quote \
    --key-context="${AK_CTX}" \
    --pcr-list="${PCR_LIST}" \
    --qualification="${nonce}" \
    --message="/tmp/quote.msg" \
    --signature="/tmp/quote.sig" \
    --format=plain \
    --hash-algorithm=sha256

cat /tmp/quote.sig /tmp/quote.msg | base64 --wrap=0 | qrencode -t ANSIUTF8
